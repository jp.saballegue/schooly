import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './home/Home.jsx';
import Landing from './landing/Landing.jsx';

// App component - represents the whole app
export default class App extends Component {
	routes() {
		return(
			<Switch>
				<Route exact path='/' component={Landing}/>
				<Route exact path='/home' component={Home}/>
			</Switch>
		);
	}
	render() {
		return (
			<div className="container-fluid container-grid">
				{this.routes()}
			</div>
		);
	}
}
