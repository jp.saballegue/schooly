import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Login extends Component {
	constructor(props) {
		super(props);
		this.input_username = React.createRef();
		this.input_password = React.createRef();
	}
	render() {
		return (
			<div className="container-login">
				<h2 className="container-header">LOGIN</h2>
				<form className="form form-login">
					<div className="form-group w-100">
						<label className="input-login-label">Username</label>
						<div className="input-login-field">
							<input type="email" className="form-control w-100" ref={this.input_username}/>
						</div>
					</div>
					<div className="form-group w-100">
						<label className="input-login-label">Password</label>
						<div className="input-login-field">
							<input type="password" className="form-control w-100" ref={this.input_password}/>
						</div>
					</div>
					
				</form>
				<div className="login-control">
					<div className="wrapper-link-signup">
						<Link to="/" className="text-secondary">
							Sign up
						</Link>
					</div>
					<div className="wrapper-btn-login">
						<button type="button" className="btn btn-primary btn-block">ENTER</button>
					</div>
					<div className="wrapper-link-forgot">
						<Link to="/" className="text-secondary">
							I forgot my login
						</Link>
					</div>					
				</div>
			</div>
		);
	}
}
