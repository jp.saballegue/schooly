import React, { Component } from 'react';
import Login from '../auth/login/Login.jsx';

export default class Landing extends Component {
	render() {
		return (
			<div className="container-landing">
				<div className="item-features">
					{/* TODO: curate features here */}
					<h1>Features</h1>
				</div>
				<div className="item-login">
					{/* TODO: user credentials functionality */}
					<Login />
				</div>
				<div className="item-ads">
					{/* TODO: ads support or self promotion */}
					<h1>Ads</h1>
				</div>
				
			</div>
		);
	}
}
