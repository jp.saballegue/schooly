import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const EstablishmentSchema = Schema({
	name: {
		type: String,
		required: true
	  },
	date_created: {
		type: Date,
		default: Date.now
	}
	
});

export const EstablishmentModel = mongoose.model('Establishment', EstablishmentSchema, 'establishment');

// Hooks
EstablishmentSchema.post('init', function(doc) {
	console.log('%s initialized', doc._id);
});
EstablishmentSchema.post('validate', function(doc) {
	console.log('%s validated', doc._id);
});
EstablishmentSchema.post('save', function(doc) {
	console.log('%s saved', doc._id);
});
EstablishmentSchema.post('remove', function(doc) {
	console.log('%s removed', doc._id);
});