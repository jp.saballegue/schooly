import { Meteor } from 'meteor/meteor';

export const Establishment = new Meteor.Collection('establishment');
Establishment.allow({
	insert: function() {
		return false;
	},
	update: function() {
		return false;
	},
	remove: function(userId, doc) {
		return false;
	}
});