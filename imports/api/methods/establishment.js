import { Establishment } from '../collections/establishment.js';
import { EstablishmentModel } from '../models/establishment.js';
import { Meteor } from 'meteor/meteor';

Meteor.methods({
	add_establishment({name}){
		return Establishment.insert({name});
	},
	create_establishment: function({name}){
		

		let asyncCreate = Meteor.wrapAsync(function(params,callback){
			let new_establishment = new EstablishmentModel({name:params.name});
			new_establishment.save().then(
				(res)=>{
					callback(null,"SUCCESS");
				},(err)=>{
					callback(err,null);
				}
			);
		});
		return asyncCreate({name});
		//return new_establishment;
	}
});