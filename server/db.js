import { Meteor } from 'meteor/meteor';
const mongoose = require('mongoose');
 
let MONGO_URL = Meteor.settings.MONGO_URL;

export const db = {
	start(){
		mongoose.connect( MONGO_URL, {
			keepAlive: true,
			reconnectTries: Number.MAX_VALUE
		});
		
		let connection = mongoose.connection;
		
		connection.on('error', (err) => console.log('Mongoose connection error: ', err));
		connection.once('open', function () {
			console.log('Mongoose connected to database!');
		});
	}
};
