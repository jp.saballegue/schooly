import { Meteor } from 'meteor/meteor';
import { db } from './db';

Meteor.startup(() => {
	// code to run on server at startup
	db.start();
	console.log(`Settings:
	MONGO_URL : ${process.env.MONGO_URL}
	MONGO_USER : ${process.env.MONGO_USER}
	MONGO_PASS : ${process.env.MONGO_PASS}`);
});
